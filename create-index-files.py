from os import listdir

files = listdir("src")
structure = {}
for md_file in files:
    if not 'md' in md_file or not len(md_file.split('-')) == 3:
        continue
    [month, day, year] = md_file.removesuffix('.md').split('-')
    for index, line in enumerate(open('src/'+md_file).readlines()):
        structure[year] = structure.get(year, {})
        structure[year][month] = structure[year].get(month, {})
        structure[year][month][day] = structure[year][month].get(day, {})
        structure[year][month][day][index] = line

summaryfile = open('src/SUMMARY.md', 'w')
summaryfile.write('[Index](SUMMARY.md)\n')
for year in structure.keys():
    yearfilename = f'{year}.md'
    yearfile = open(f'src/{yearfilename}', 'w')
    yearfile.write(f'# {year}\n')
    summaryfile.write(f'- [{year}]({yearfilename})\n')
    for month in structure[year].keys():
        monthfilename = f'{month}-{year}.md'
        monthfile = open(f'src/{monthfilename}', 'w')
        monthfile.write(f'# {month}/{year}\n')
        summaryfile.write(f'  - [{month}/{year}]({monthfilename})\n')
        yearfile.write(f'- [{month}/{year}]({monthfilename})\n')
        for day in structure[year][month].keys():
            dayfilename = f'{month}-{day}-{year}.md'
            summaryfile.write(f'    - [{month}/{day}]({dayfilename})\n')
            yearfile.write(f'  - [{month}/{day}]({dayfilename})\n')
            monthfile.write(f'- [{month}/{day}]({dayfilename})\n')
            for line in structure[year][month][day].values():
                summaryfile.write(f'      {line}')
                yearfile.write(f'    {line}')
                monthfile.write(f'  {line}')
#print(structure)


