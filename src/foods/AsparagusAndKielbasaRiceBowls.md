# Asparagus And Kielbasa Rice Bowls

* No recipe for this one
    * We fried up some kielbasa, grilled some asparagus with a little oil and salt, and drizzled hot pepper oil over the asparagus

* I took a decent amount of the solids from the oil and found that the dish was nicely balanced
* Rachel went lighter on the oil and avoided the solids. She found that it was rather hot for her tastes.
    * The kielbasa and rice complemnted each other surprisingly well
    * Without the solids from the oil there was no delecious pepper flavor, just pain.
