 
# Sweet Potato In Tomato Lime And Cardamom Sauce

* From "Flavor" by Ottolenghi

* Tastes good, and is very simple
* We cheated a bit by not blending the sauce. It wasn't quite as pretty, but tasted just as good.
* A little heavy on the cardamom in the sauce, which gave it a nice depth of flavor.
* The sweet potato was a little difficult to eat at the beginning with just a fork, due to the skins. This was largely related to serving it in deep sided pasta plates.
    * Cutting the potato into half-moons would have fixed this.
