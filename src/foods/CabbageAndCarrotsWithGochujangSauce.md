# Cabbage and Carrots with Gochujang sauce

* From "East" by Meera Sodha

* We have made this tons of times, it is easy and delicious.
* Letting the cabbage get a bit crispy is great.
* The sauce always seems like not enough when preparing it.
